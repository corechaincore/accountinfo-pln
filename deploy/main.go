package main

import (
	"net/http"
	"bytes"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"encoding/json"
	"strings"
)

type accounts struct {
	Rescode string `json:"rescode"`
	Data accountsDetail `json:"data"`
	Message accountsDetail2 `json:"message"`
}
type accountsDetail struct {
	Type    string           `json:"type"`
	Inquiry []accountsDetail2 `json:"inquiry"`
}
type accountsDetail2 struct {
	Title string `json:"title"`
	Body string `json:"body"`
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/apiv1/pln/infoaccount", getName())
	http.ListenAndServe(":6030", mux)
}

func validation(customerNumber string,types string)(err bool){
	err = true
	if(len(customerNumber)<=0){
		err = false
	}else if(len(types)<=0){
		err = false
	}
	return err
}

func getName() func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request){
		status := http.StatusOK
		message := ""
		body, _ := ioutil.ReadAll(r.Body)
		customerNumber, _ := jsonparser.GetString(body, "customer_number")
		types, _ := jsonparser.GetString(body, "payment_type")
		if(validation(customerNumber,types)){
			productCode := "282"
			if(strings.Compare(strings.ToLower(types),"prabayar") == 0){
				productCode = "282"
			}else{
				productCode = "614"
			}

			client := &http.Client{}
			bodyParam := `{"phone_number":"","customer_number":"`+customerNumber+`","product_id": "`+productCode+`"}`
			req, err := http.NewRequest("POST", "https://gaia.sepulsa.com/bumi/pln_services/inquiry?1516112537", bytes.NewBuffer([]byte(bodyParam)))
			if err 	!= nil {
				status = http.StatusInternalServerError
				message = `{"message":"` + err.Error() + `"}`
			}
			req.Header.Set("Content-Type", "application/json")
			resp, err := client.Do(req)
			response := accounts{}
			bodyBytes, _ := ioutil.ReadAll(resp.Body)
			if err == nil {
				statusCode := resp.StatusCode
				err = json.Unmarshal(bodyBytes, &response)
				if err == nil {
					if statusCode == http.StatusCreated || statusCode == http.StatusOK {
						msg := response.Message.Body
						if(strings.ContainsAny(strings.ToLower(msg),"lunas")){
							status = http.StatusNotFound
							message = `{"message":"` + msg + `"}`
						}else{
							if(strings.Compare(strings.ToLower(types),"prabayar") == 0){
								nameTemp := strings.Replace(response.Data.Inquiry[0].Body,"<b>","",-1)
								nameTemps := strings.Replace(nameTemp,"</b>","",-1)
								tarifDaya := strings.Replace(response.Data.Inquiry[1].Body,"<b>","",-1)
								tarifDayas := strings.Replace(tarifDaya,"</b>","",-1)
								arrayTarifDaya := strings.Split(tarifDayas,"/")
								tarif := arrayTarifDaya[0]
								daya := arrayTarifDaya[1]
								message = `{"customer_name":"` + strings.Trim(nameTemps,"")+ `", "tariff":"` + strings.Trim(tarif,"")+ `","power":"` + strings.Trim(daya,"")+ `"}`
							}else{
								nameTemp := strings.Replace(response.Data.Inquiry[0].Body,"<b>","",-1)
								nameTemps := strings.Replace(nameTemp,"</b>","",-1)
								period := strings.Replace(response.Data.Inquiry[1].Body,"<b>","",-1)
								periods := strings.Replace(period,"</b>","",-1)
								bill := strings.Split(response.Data.Inquiry[3].Body,"<b>")[1]
								bills := strings.Split(bill,"</b>")[0]
								message = `{"customer_name":"` + strings.Trim(nameTemps,"")+ `","period":"` + strings.Trim(periods,"")+ `","bill":"` + strings.Trim(bills,"")+ `"}`
							}
						}
					}else{
						status = http.StatusInternalServerError
						message = `{"message":"Gagal Ambil Data"}`
					}
				}else {
					status = http.StatusInternalServerError
					message = `{"message":"` + err.Error() + `"}`
				}
			}else {
				status = http.StatusInternalServerError
				message = `{"message":"` + err.Error() + `"}`
			}
		}else{
			status = http.StatusBadRequest
			message = `{"message":"Parameter Inputan Salah"}`
		}

		w.WriteHeader(status)
		w.Write([]byte(message))
	}
}